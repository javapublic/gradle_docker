#!/bin/bash
export LC_ALL="en_US.UTF-8"

do_docker=1
do_command=help
if [ "$#" -gt 0 ]; then
    do_command=$1
fi

case $do_command in
  run)
    run_dir='static-ip'
    run_opts='-f docker-compose-template.yml '

    if [ "$#" == 1 ] || [ "$2" == "static-ip" ]; then

        run_dir="static-ip"
        run_opts=$run_opts" -f static-ip/network.yml up -d"
        do_docker=0

    elif [ "$2" == "bridge" ]; then

        run_dir=$2
        run_opts=$run_opts" up -d"
        do_docker=0

    elif [ "$2" == "host" ]; then

        run_dir=$2
        run_opts=$run_opts" up -d"
        do_docker=0

    else
        echo "illegal option"
        do_docker=2
    fi

    if [ $do_docker == 0 ]; then
        echo "==========stop original containers========="
        docker stop gitlab nginx bind registrymirror jenkins
        echo "==========remove original containers========="
        docker rm gitlab nginx bind registrymirror jenkins
        rm -rf curr
        echo "==========link env to [$run_dir]========="
        ln -s $run_dir curr
        echo "==========build new containers========="
        docker-compose $run_opts
        exit 0
    fi


    ;&

  stop)
    docker stop gitlab nginx bind registrymirror jenkins
    exit 0
    ;&
  start)
    docker start gitlab nginx bind registrymirror jenkins
    exit 0
    ;&
  restart)
    docker restart gitlab nginx bind registrymirror jenkins
    exit 0
    ;&
  remove)
    docker rm gitlab nginx bind registrymirror jenkins
    exit 0
    ;&
  build)
    build_dir="jenkins_image"
    tag_name="sean/jenkins:latest"
    if [ "$#" -eq 1 ] || [ "$2" == "jenkins" ]; then
        build_dir="jenkins_image"
        tag_name="sean/jenkins:latest"
        docker stop jenkins
        docker rm jenkins
        do_docker=0
    elif [ "$2" == "gitlab" ]; then
        build_dir="jenkins_image"
        tag_name="sean/gitlab:latest"
        docker stop gitlab
        docker rm gitlab
        do_docker=0
    else
        echo "illegal option"
        do_docker=2
    fi
 
    if [ $do_docker == 0 ]; then
        curr_path="$(pwd)"
        cd $build_dir
        docker rmi $tag_name
        echo "==========build images[$tag_name]========="
        docker build -t $tag_name .
        cd $curr_path
        exit 0
    fi


    ;&


  help)
    do_docker=9999
    ;;
esac


if [ "$do_docker" != 9999 ]; then
    echo "illegal command: ${do_command}"
fi
echo "=> do_docker.sh [command] [option]"

echo "   commands:"
echo "      run       #containers"
echo "      stop      #containers"
echo "      start     #containers"
echo "      restart   #containers"
echo "      remove    #containers"
echo "      build     #images"
echo ""
echo "   options for run:"
echo "      static-ip (default)"
echo "      bridge"
echo "      host"
echo "   options for build:"
echo "      jenkins"
echo "      gitlab"

if [ "$do_docker" != 9999 ]; then
    exit 1
fi

exit 0



