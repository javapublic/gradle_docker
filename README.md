# README #

* A continuous integration project with jenkins, gitlab, gradle and docker. 

### Prerequisite ###

* Version
 
      docker images:

        sameersbn/bind:latest
        hyper/docker-registry-web:latest
        nginx:alpine
        gitlab/gitlab-ce:8.10.0-rc1.ce.0
        registry:2
      
      docker:

        Docker version 1.11.2, build b9f10c9
        docker-compose version 1.8.0-rc1, build 9bf6bc6
    
      gradle: 2.14 / 2.14.1
      java : 1.8
      
    
* Architecture
  
        [Dev] --checkin--> [gitlab] --gitlab hook--> [Jenkins] ---> docker build -> [registry]
                                                                                     |
                                                                                     |
                                                                 docker run <--pull--+
      
        Mac
         |
         +---192.168.56.1-- VirtualBox Host(10.0.2.15)
                                  |
                                  +--10.0.0.1---+---- bind (10.0.0.100)
                                                |
                                                +---- jenkins (10.0.0.102)
                                                |
                                                +---- registry (10.0.0.103)
                                                |
                                                +---- registry-web (10.0.0.104)
                                                |
                                                +---- mysql (10.0.0.105)
                                                |
                                                +---- nginx (10.0.0.200)


### Implementation ###
* Build a jenkins images  

    1. Build a jenkins with centos7

            FROM centos:7
      
            #MAINTAINER SDiedel <stefan@diedel.net>
            MAINTAINER Sean S.Y. Lo
      
            ENV JDK_PACKAGE java-1.8.0-openjdk-devel
            RUN yum -y install ${JDK_PACKAGE} git libtool-ltdl
            RUN echo "export JAVA_HOME=/etc/alternatives/java_sdk" >> /etc/bashrc
            
            
            #ENV JENKINS_VERSION 1.638
            #ENV JENKINS_SHA 7f494be16a8769d089651b8c8205b85e34f2c9cc
            #RUN mkdir /opt/jenkins && \
            #    mkdir -p /opt/data/jenkins/war && \
            #    curl -fL http://mirrors.jenkins-ci.org/war/$JENKINS_VERSION/jenkins.war --output /opt/jenkins/jenkins.jar && \
            #    echo "$JENKINS_SHA /opt/jenkins/jenkins.jar" | sha1sum -c -
            #
            #ENV JENKINS_HOME /opt/data/jenkins_home
            #
            #RUN useradd -d "$JENKINS_HOME" -m -s /bin/bash jenkins 
            #VOLUME /opt/data/jenkins_home
            
            ENV JENKINS_VERSION 2.13
            
            RUN mkdir /opt/jenkins && \
                mkdir -p /opt/data/jenkins/war && \
                curl -fL http://mirrors.jenkins-ci.org/war/$JENKINS_VERSION/jenkins.war --output /opt/jenkins/jenkins.jar
            
            ENV JENKINS_HOME /var/jenkins_home
            
            #don't create user @ $JENKINS_HONE ,once host bind volume as then folder, the process could not load bashrc from /etc/bashrc (~.             bashrc could not be loaded) 
            #RUN useradd -d "$JENKINS_HOME" -u 1000 -m -s /bin/bash jenkins 
            RUN useradd -d "/home/jenkins" -u 1000 -m -s /bin/bash jenkins 
            
            RUN mkdir -p $JENKINS_HOME; chown -R jenkins.jenkins $JENKINS_HOME
            
            VOLUME $JENKINS_HOME
            
            EXPOSE 8080
            
            USER jenkins
            
            CMD java -Xms512m -Xmx1024m -jar /opt/jenkins/jenkins.jar
   
    2. setup a my-own jenkins images
 
            #FROM jenkinsci/jenkins:2.13
            #FROM sean/jenkins-ubuntu
            FROM sean/jenkins-centos
            
            USER root
            
            COPY plugins.txt /usr/share/jenkins/plugins.txt
            
            #------------you don't need to do time sync, container will refer to host -----------------
            #RUN apt-get update && \
            #    apt-get upgrade -y && \
            #    apt-get install -y ntpdate
            
            #RUN echo "ntpdate ntp.ubuntu.com" > /etc/cron.daily/ntpdate.sh 
            #RUN chmod 755 /etc/cron.daily/ntpdate.sh
            #RUN echo "ntpdate ntp.ubuntu.com" >> /etc/rc.local
            
            #------------setting timezone here globally, when you type "date" command -----------------
            #RUN echo "Asia/Taipei" > /etc/timezone && dpkg-reconfigure --frontend noninteractive tzdata
            
            #RUN /usr/local/bin/plugins.sh /usr/share/jenkins/plugins.txt
            
            ENV USER_BIN "/usr/local"
            ENV MAVEN_VERSION "3.3.9"
            ENV GRADLE_VERSION "2.14"
            
            #COPY apache-maven-${MAVEN_VERSION}-bin.tar.gz ${USER_BIN}/maven.tar.gz
            COPY apache-maven-${MAVEN_VERSION} ${USER_BIN}/maven
            COPY gradle-${GRADLE_VERSION} ${USER_BIN}/gradle
            
            #for ubuntu
            #RUN echo "export PATH=\$PATH:${USER_BIN}/maven/bin:${USER_BIN}/gradle/bin" >>  /etc/bash.bashrc
            #RUN echo "export GRADLE_HOME=${USER_BIN}/gradle" >>  /etc/bash.bashrc
            
            #for centos
            RUN echo "export PATH=\$PATH:${USER_BIN}/maven/bin:${USER_BIN}/gradle/bin" >>  /etc/bashrc


* Install docker-compose

            curl -L https://github.com/docker/compose/releases/download/1.8.0-rc1/docker-compose-`uname -s`-`uname -m` > /usr/local/bin/docker- compose
            chmod +x /usr/local/bin/docker-compose


* Self-signed certification and basic authentication file for nginx

            please refer to
              https://github.com/innobead/docker-registry-samples
              https://docs.docker.com/registry/deploying/

* Source code

            A test project(testprj), groovy style, periodically print a time string.
            The project included multiple nested project.


* Gradle

           [testprj]
             +-- build.gradle       //top project, docker settings 
             +-- setting.gradle     //subprojects
             |
             +-----[timeUtils]
             |         +-- build.gradle     //timeUtils' dependences 
             |
             +-----[timeStringUtils]
             |         +-- build.gradle     //timeStringUtils' dependences
             |
             +-----[periodicallyTimeString]
                       +-- build.gradle     //mainClass


* Gitlab hook(image: gitlab/gitlab-ce:8.10.0-rc1.ce.0)

            create a gitlab repository, and set webhook up.
![螢幕快照 2016-07-31 下午7.06.29.png](https://bitbucket.org/repo/6BqAbB/images/2515694187-%E8%9E%A2%E5%B9%95%E5%BF%AB%E7%85%A7%202016-07-31%20%E4%B8%8B%E5%8D%887.06.29.png)


* Jenkins 

    1. install global module
         gradle 2.14
![螢幕快照 2016-07-31 下午7.08.09.png](https://bitbucket.org/repo/6BqAbB/images/2697400074-%E8%9E%A2%E5%B9%95%E5%BF%AB%E7%85%A7%202016-07-31%20%E4%B8%8B%E5%8D%887.08.09.png)

![螢幕快照 2016-07-31 下午7.08.26.png](https://bitbucket.org/repo/6BqAbB/images/3516068451-%E8%9E%A2%E5%B9%95%E5%BF%AB%E7%85%A7%202016-07-31%20%E4%B8%8B%E5%8D%887.08.26.png)

    2. install following plugins:
         gitlab plugin
         gitlab hook pluglin
         gradle plugin

![螢幕快照 2016-07-31 下午7.09.35.png](https://bitbucket.org/repo/6BqAbB/images/1694403941-%E8%9E%A2%E5%B9%95%E5%BF%AB%E7%85%A7%202016-07-31%20%E4%B8%8B%E5%8D%887.09.35.png)

    3. create a free-style project, setup you source code control ,other you need and gitlab-releated:
         checked "遠端觸發建置 (例如: 透過 Script 腳本)" and filled the "驗證 Token" what you used in gitlab
         checked "Build when a change is pushed to GitLab. GitLab CI Service URL: http://jenkins.com/project/javaPublic_docker       ". you may used URL from the statement of checkbox(for web hook)
 
    4. Invoke Gradle script for build
         Gradle version : gradle-2.14 ( what you install in global module)
         Tasks: clean build buildDocker
         Build File: testprj/build.gradle 


![螢幕快照 2016-07-31 下午7.09.57.png](https://bitbucket.org/repo/6BqAbB/images/3829047003-%E8%9E%A2%E5%B9%95%E5%BF%AB%E7%85%A7%202016-07-31%20%E4%B8%8B%E5%8D%887.09.57.png)

    5.shell (clean unused images after docker build)
         docker images --no-trunc | grep none | awk '{print $3}' | xargs docker rmi

* Build and run 
![螢幕快照 2016-07-31 下午4.42.24.png](https://bitbucket.org/repo/6BqAbB/images/1755944497-%E8%9E%A2%E5%B9%95%E5%BF%AB%E7%85%A7%202016-07-31%20%E4%B8%8B%E5%8D%884.42.24.png)

![螢幕快照 2016-07-31 下午4.44.45.png](https://bitbucket.org/repo/6BqAbB/images/148105722-%E8%9E%A2%E5%B9%95%E5%BF%AB%E7%85%A7%202016-07-31%20%E4%B8%8B%E5%8D%884.44.45.png)


* Integration Jenkins with Hipchat  

    1. Generate a token from Hipchat
         Create a hipchat room, and then create a api access token, 
             [Edit profile] -> [API access] -> after password confirmed -> you could get a [API token]

         also remember your [Hipchat Room name]

    2. Config Jenkins project

            First install the Hipchat plugin: Manage Jenkins > Manage Plugins > Available > and search for the Hipchat plugin.

            Restart the Jenkins instance.

            Config Jenkins [System Setting] as

![螢幕快照 2016-08-01 下午3.07.09.png](https://bitbucket.org/repo/6BqAbB/images/3625297730-%E8%9E%A2%E5%B9%95%E5%BF%AB%E7%85%A7%202016-08-01%20%E4%B8%8B%E5%8D%883.07.09.png)

        Change [Project Setting] as
![螢幕快照 2016-08-01 下午3.07.41.png](https://bitbucket.org/repo/6BqAbB/images/3877392089-%E8%9E%A2%E5%B9%95%E5%BF%AB%E7%85%A7%202016-08-01%20%E4%B8%8B%E5%8D%883.07.41.png)

    3. Messages in HipChat
![螢幕快照 2016-08-01 下午3.16.48.png](https://bitbucket.org/repo/6BqAbB/images/2237573138-%E8%9E%A2%E5%B9%95%E5%BF%AB%E7%85%A7%202016-08-01%20%E4%B8%8B%E5%8D%883.16.48.png)